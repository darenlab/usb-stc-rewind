/**
 * USB Related Code
*/

#ifndef _USB_H_
#define _USB_H_
void usb_init();
void handler_pm();
void handler_request();

#endif