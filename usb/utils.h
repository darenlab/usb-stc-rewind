/**
 * USB Utils Code
*/

#ifndef _USB_UTILS_
#define _USB_UTILS_

void usb_reg_write(unsigned char addr, unsigned char value);
unsigned char usb_reg_read(unsigned char addr);

void usb_fifo_write(unsigned char fifo_addr, unsigned char *buffer);
unsigned char* usb_fifo_read(unsigned char fifo_addr);

#endif